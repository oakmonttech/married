/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { createStore } from 'redux'
import { Provider } from 'react-redux';
import marriedApp from './App/reducers/reducers'
let store = createStore(marriedApp, window.STATE_FROM_SERVER)

import {
  AppRegistry,
} from 'react-native';

import SceneContainer from './App/containers/SceneContainer'


export default class Married extends Component {
  render() {
    return (
      <Provider store = {store} >
        <SceneContainer></SceneContainer>
      </Provider>
    );
  }
}



AppRegistry.registerComponent('Married', () => Married);
