import { combineReducers } from 'redux'
import { CHANGE_ITEM_BUDGET } from '../actions/actions'


const budget = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_ITEM_BUDGET':
      return {
        budgetCategory: action.budgetCategory,
        amount: action.amount,
      }
    case 'CHANGE_ITEM_BUDGET':
      if (state.budgetCategory !== action.budgetCategory) {
        return state
      }

      return Object.assign({}, state, {
        amount: action.amount
      })
  }
}

const budgets = (state = [], action) => {
  switch (action.type) {
    case 'ADD_ITEM_BUDGET':
      return [
        ...state,
        budget(undefined, action)
      ]
      case 'CHANGE_ITEM_BUDGET':
      return state.map(b =>
      budget(b, action)
    )
    default:
        return state
    }

}

const marriedApp = combineReducers({
  budgets,
})

module.exports = marriedApp;
