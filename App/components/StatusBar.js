'use strict';
import React, {Component} from 'react';
import ReactNative from 'react-native';

const { StyleSheet, Text, View} = ReactNative;

class StatusBar extends Component {
  render() {
    return (
      <View>
        <View style={styles.statusbar}/>
        <View style={styles.navbar}>
          <Text style={styles.navbarTitle}>{this.props.title}</Text>
        </View>
      </View>
    );
  }
}

//we will have to change these to meet our needs
const styles = StyleSheet.create({
  navbar: {
    alignItems: 'center',
    backgroundColor: '#60DFE5',
    borderColor: 'transparent',
    borderWidth: 1,
    justifyContent: 'center',
    height: 44,
    flexDirection: 'row'
  },
  navbarTitle: {
    color: 'white',
    fontSize: 16,
    fontWeight: "500"
  },
  statusbar: {
    backgroundColor: '#60DFE5',
    height: 22,
  },
});



module.exports = StatusBar;
