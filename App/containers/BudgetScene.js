/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
  TouchableHighlight,
} from 'react-native';

import { toggleTodo,setVisibilityFilter} from '../actions/actions'
import marriedApp from '../reducers/reducers'
import {addNewAmount} from '../actions/actions'
import {changeBudgetAmount} from '../actions/actions'
import { connect } from 'react-redux';
import Budget from './Budget'


class BudgetScene extends Component {
  constructor(props) {
    super(props);
  }

  componentWillReceiveProps() {
    console.log('componenet receiving props');
  }


  render() {
    const {
      budgets,
      addNewAmount,
      changeBudgetAmount,

    } = this.props;
    console.log('The budgets are',budgets);
    return (

          <Budget budgets={budgets} addNewAmount ={addNewAmount} changeBudgetAmount={changeBudgetAmount} style= {{flex:1}}>
          </Budget>

    );
  }
}

const mapStateToProps = (state) => {
  console.log('mappping the budgets',state.budgets);
  return {
    budgets: state.budgets
  };
};

const mapDispatchToProps = (dispatch) => {
  console.log('mappping props to state');
  return {
    addNewAmount: (title,amount) => {
      dispatch(addNewAmount(title,amount));
    },
    changeBudgetAmount: (title,amount) => {
      dispatch(changeBudgetAmount(title,amount));
    }
  };
};

const BudgetSceneContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(BudgetScene);

export default BudgetSceneContainer;
