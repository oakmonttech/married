

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  TextInput,
} from 'react-native';

import BudgetInfo from '../budgetData/BudgetInfo.js'
import StatusBar from '../components/StatusBar.js'
const budgetInfo = new BudgetInfo();

export default class Budget extends Component {


  constructor(props){
    super(props);
    this.onPress = this.onPress.bind(this);
    this.props.budgets = []

    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 != r2
    });

    this.state = {
        dataSource: ds.cloneWithRows(Object.keys(budgetInfo.weddingItemsAndProjectedBudget())),
        ds: Object.keys(budgetInfo.weddingItemsAndProjectedBudget()),
    };

  }

  updateDataSource(budgets) {
    this.props.budgets = budgets
    console.log('the budgets are',budgets);
    var ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 != r2
    });
    this.setState({
      dataSource: ds.cloneWithRows(Object.keys(budgetInfo.weddingItemsAndProjectedBudget())),
      ds: Object.keys(budgetInfo.weddingItemsAndProjectedBudget()),
    });
  }

  componentWillReceiveProps(newProps) {
    this.updateDataSource(newProps.budgets);
  }

  render() {


    return (
      <View style={styles.container}>
      <StatusBar/>
      <ListView
          dataSource={this.state.dataSource}
          renderRow={this.renderItem.bind(this)}
          enableEmptySections={true}
          style={styles.tableView}/>
      </View>

    );
  }

  renderItem(category) {
    var value = ""

    value = budgetInfo.valueForBudgetCategory(category,this.props.budgets)
    
    return (
      <View style = {styles.itemContainer}>
        <View style = {styles.item}>
          <Text style = {styles.text}>{category}</Text>
          <View style = {{flex:1}}/>
          <TextInput
          placeholderTextColor= 'white'
          placeholder= "Amount"
          style= {styles.textInput}
          onChangeText = {(text)=> this.onPress(category,text)}
          value = {value}
          category={category}
         ></TextInput>

        </View>
      </View>
    );
  }

  onPress(category,amount){

    const {addNewAmount,changeBudgetAmount,budgets} = this.props;
    console.log('The category and amount',category,amount);
    if (budgetInfo.budgetDictForCategory(category,budgets) != null){
      changeBudgetAmount(category,amount)
    }else{
      addNewAmount(category,amount)
    }

  }

  getMeasurement() {
    const width = Dimensions.get('window').width * 0.8;
    const height = Dimensions.get('window').height;
    return width > height ? height / 7.0 : width / 7.0;
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: 'white',
  },
  tableView:{
    flex:1,
    backgroundColor:'transparent',
  },
  item:{
    flex:1,
    alignItems:'center',
    flexDirection: 'row',
  },
  itemContainer:{
    flex:1,
    paddingTop:30,
    paddingBottom:30,
  },
  text:{
    color:'white',
    backgroundColor:'blue',
    left:20,
    fontSize: 24,
  },
  textInput:{
    width:80,
    height:40,
    backgroundColor:'green',
    textAlign:'center',
    textAlignVertical:'center',
    right:20,
  },
});
