import React, { Component } from 'react';
import {
  Navigator,
  Image,
} from 'react-native';
import BudgetSceneContainer from './BudgetScene.js';

export default class Navigation extends Component {
  configureScene() {
    return {
      ...Navigator.SceneConfigs.PushFromRight,
      gestures: {}
    };
  }

  renderScene(route, navigator) {
    if (route.component) {
      return React.createElement(route.component, { navigator, ...route.passProps });
    }
  }

  render() {


    return (
      <Image style={{flex:1}} source={{uri:'https://images.pexels.com/photos/30732/pexels-photo-30732.jpg?w=940&h=650&auto=compress&cs=tinysrgb'}}>
        <Navigator
          initialRoute={{name: 'BudgetSceneContainer', component: BudgetSceneContainer}}
          configureScene={this.configureScene}
          renderScene={this.renderScene}
        />
      </Image>
    );
  }
}
