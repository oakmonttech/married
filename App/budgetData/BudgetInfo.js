

import React from 'react';


class BudgetInfo{


 weddingItemsAndProjectedBudget(){
    return {
      //can we set this to nil
      "Total Budget" : 0,
      "Remaining Budget" : 0,
      "Wedding Gown" : 0.06,
      "Hair and Makeup" : 0.02,
      "Bridal Accessories" : 0.01,
      "Attire Misc." : 0.1,
      "Venue Rental" : 0.08,
      "Reception Food Service" : 0.25,
      "Reception Beverages" : 0.14,
      "Cake" : 0.05,
      "Reception Misc." : 0.01,
      "Ceremony Flowers": 0.02,
      "Wedding Bouqets" : 0.01,
      "Reception Flowers" : 0.04,
      "Floral Misc." : 0.01,
      "Ceremony Venue Rental" : 0.02,
      "Officiant" : 0.01,
      "Ceremony Misc" : 0.01,
      "Photography" : 0.06,
      "Videography" : 0.04,
      "Photography Misc." : 0.02,
      "Ceremony Music" : 0.01,
      "Cocktail Music" : 0.01,
      "Reception Music" : 0.06,
      "Music Misc.": 0.01,
      "Invitations" : 0.02,
      "Programs" : 0.01,
      "Stationary Misc." : 0.01,
    }
  }

  valueForBudgetCategory(category,budgets){

    if (this.budgetDictForCategory(category,budgets) != null){
      return this.budgetDictForCategory(category,budgets)
    }
    return ""
  }

  budgetDictForCategory(category,budgets){
    for ( const key in budgets ) {
      if (budgets[key].budgetCategory === category){
        return budgets[key].amount
      }
    }
    return null
  }
}

module.exports = BudgetInfo;
