/*
 * action types
 */

export const CHANGE_ITEM_BUDGET = 'CHANGE_ITEM_BUDGET'
export const ADD_ITEM_BUDGET = 'ADD_ITEM_BUDGET'

/*
 * action creators
 */
export const addNewAmount = (category,amount) => {
  return {
    type: ADD_ITEM_BUDGET,
    budgetCategory: category,
    amount:amount,
  }
}

export const changeBudgetAmount = (category,amount) => {
  return {
    type: CHANGE_ITEM_BUDGET,
    budgetCategory: category,
    amount:amount,
  }
}
